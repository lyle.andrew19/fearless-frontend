import { useState } from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';


import LocationForm from './LocationForm';


import ConferenceForm from './ConferenceForm';
import PresentationForm from "./PresentationForm";

import MainPage from "./MainPage";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />


        <Route path="/locations/new" element={<LocationForm />} />

        <Route path="conferences/new" element={<ConferenceForm />} />

        <Route path="/presentation/new" element={<PresentationForm />} />

      </Routes>
    </BrowserRouter>
  );
}

export default App;
